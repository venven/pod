/***************************************************************************/ /**
 * \file setpoint_node.cpp
 *
 * \brief Node that publishes time-varying setpoint values
 * \author Paul Bouchier
 * \date January 9, 2016
 *
 * \section license License (BSD-3)
 * Copyright (c) 2016, Paul Bouchier
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * - Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 * - Neither the name of Willow Garage, Inc. nor the names of its contributors
 * may be used to endorse or promote products derived from this software
 * without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include "ros/ros.h"
//#include "std_msgs/Float64.h"
#include "geometry_msgs/Vector3Stamped.h"

#define EVAL_NUM 120	// evaluation number for each data size
double publish_time[EVAL_NUM];

int main(int argc, char** argv)
{
  ros::init(argc, argv, "setpoint_node");
  ROS_INFO("Starting setpoint publisher");
  ros::NodeHandle setpoint_node;

  while (ros::Time(0) == ros::Time::now())
  {
    ROS_INFO("Setpoint_node spinning, waiting for time to become non-zero");
    sleep(1);
  }

  //std_msgs::Float64 setpoint;
  geometry_msgs::Vector3Stamped setpoint_stamped;

  //setpoint.data = 1.0;
  //setpoint.data = 0.0;

  setpoint_stamped.vector.x = 0;
  //ros::Publisher setpoint_pub = setpoint_node.advertise<std_msgs::Float64>("setpoint", 1);
  ros::Publisher setpoint_pub = setpoint_node.advertise<geometry_msgs::Vector3Stamped>("setpoint", 1);

  ros::Rate loop_rate(100);  // change setpoint every 5 seconds

  std::string output_filename;
  output_filename = "./temp.txt";
  struct timespec tp1;		// for clock
  FILE *fp; // for file io
  int count = 0;
  while (ros::ok())
  {
    ros::spinOnce();
    
    if(count < EVAL_NUM){// 時刻の記録
      if(clock_gettime(CLOCK_REALTIME,&tp1) < 0){
        perror("clock_gettime begin");
        return 0;
      }
      publish_time[count] = (double)tp1.tv_sec + (double)tp1.tv_nsec/ (double)1000000000L;
    }
    else if(count == EVAL_NUM){
      if((fp = fopen(output_filename.c_str(), "w")) != NULL){
        for(int i=0; i<EVAL_NUM; i++){
          if(fprintf(fp, "%18.9lf\n", publish_time[i]) < 0){
            //書き込みエラー
            printf("error : can't output publish_time.txt");
            break;
          }
        }
        fclose(fp);
      }else{
        printf("error : can't output publish_time.txt");	
      }
    }
    count++;

    setpoint_stamped.header.stamp = ros::Time::now();
    setpoint_pub.publish(setpoint_stamped);  // publish twice so graph gets it as a step
    //setpoint.data = 0 - setpoint.data;
    //setpoint_pub.publish(setpoint);

    loop_rate.sleep();
  }
}
