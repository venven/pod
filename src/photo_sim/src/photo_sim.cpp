#include "ros/ros.h"
#include "std_msgs/Float64.h"

int main(int argc, char** argv)
{
  ros::init(argc, argv, "state_node");
  ROS_INFO("Starting state publisher");
  ros::NodeHandle state_node;

  while (ros::Time(0) == ros::Time::now())
  {
    ROS_INFO("state_node spinning, waiting for time to become non-zero");
    sleep(1);
  }

  std_msgs::Float64 state;
  //state.data = 1.0;
  state.data = 0.5;
  //ros::Publisher state_pub = state_node.advertise<std_msgs::Float64>("state", 1);
  ros::Publisher state_pub = state_node.advertise<std_msgs::Float64>("state", 1);

  ros::Rate loop_rate(100);  // change state every 5 seconds

  while (ros::ok())
  {
    ros::spinOnce();

    state_pub.publish(state);  // publish twice so graph gets it as a step
    //state.data = 0 - state.data;
    //state_pub.publish(state);

    loop_rate.sleep();
  }
}
