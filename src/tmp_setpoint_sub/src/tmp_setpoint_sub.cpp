#include "ros/ros.h"
//#include "std_msgs/Float64.h"
#include "geometry_msgs/Vector3Stamped.h"

void callback(const geometry_msgs::Vector3Stamped msg)
{

}

int main(int argc, char** argv)
{
  ros::init(argc, argv, "setpoint_sub");
  ROS_INFO("Starting setpoint publisher");
  ros::NodeHandle setpoint_node;

  while (ros::Time(0) == ros::Time::now())
  {
    ROS_INFO("Setpoint_node spinning, waiting for time to become non-zero");
    sleep(1);
  }

  //std_msgs::Float64 setpoint;
  //geometry_msgs::Vector3Stamped setpoint_stamped;

  //setpoint.data = 1.0;
  //setpoint.data = 0.0;

  //setpoint_stamped.vector.x = 0;
  //ros::Publisher setpoint_pub = setpoint_node.advertise<std_msgs::Float64>("setpoint", 1);
  //ros::Publisher setpoint_pub = setpoint_node.advertise<geometry_msgs::Vector3Stamped>("setpoint", 1);
  ros::Subscriber sub = setpoint_node.subscribe("setpoint", 1000, callback);

  ros::spin();

}
